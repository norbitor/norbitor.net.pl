Title: Praktykanci w boju #4
Date: 2013-05-18 02:03
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-4
islug: prwb4

Koniec drugiego tygodnia praktyk. Zapał do pracy nam nie minął, choć ostatnie trzy dni polegały raczej na
monotonnej, lecz koniecznej pracy. A to dlatego, że matura z informatyki już w najbliższą środę a do wtorku
wszystko musi być na tip‑top.

# Środa

Środa to pierwszy dzień maratonu przedmaturalnego. Poznaliśmy jeszcze bliżej Intel Platform Administration
Technology, zaczęliśmy przygotowywać bazę dla lustra w Pracowni 4 oraz żeby było trochę ciekawiej naprawiliśmy
jeden napęd.

## IPAT

Rankiem na rozgrzewkę poszliśmy do Pracowni 1 pobawić się IPATem. Przyznamy szczerze, że to
sprzętowo-programowe rozwiązanie przypadło nam do gustu i postanowiliśmy zająć się tematem później. Samo
narzędzie jest bardzo ciekawe – wyświetla nam listę komputerów w sieci, ich stan oraz daje możliwość
włączenia, wyłączenia komputera, zrobienia lustra, czy też ustawienia, aby wybrany komputer udostępniał swój
dysk jako baza dla innego urządzenia w sieci.

Z tymi backupami jest jednak tak, że nie działa to na zasadzie bit w bit, tylko trochę bardziej
inteligentnie. Z obrazu usuwane są informacje o kluczu produktu, czy aktywacji Windowsa, ponadto automatycznie
ustawiana jest właściwa nazwa komputera w sieci. Niby nic, ale (no poza aktywacją) cieszy. Poniżej
„screenphoto” tegoż narzędzia (tak, wiem, informatycy, a screenshota nawet nie zrobili – uwierzcie jednak,
nawet Paint na tym dziwnym serwerze ładuje się kilkadziesiąt sekund - oszałamiająca prędkość odczytu z HDD: 3
MB/s).

<figure>
  <img src="{static}/images/pw4-panel-ipat.webp" alt="Zdjęcie ekranu z głównym panelem webowym narzędzia IPAT, widoczna lista 16 komputerów">
  <figcaption>Okno główne IPAT.</figcaption>
</figure>

## Maraton przedmaturalny czas zacząć

Pobawiliśmy się trochę platformą Intela, ale musieliśmy przerwać nasze zapędy poznawcze, gdyż „robota
czeka”. Jak się później okazało robota nie trudna, ale niesamowicie pożerająca wręcz czas.

Wybraliśmy się do Pracowni 4, żeby zapuścić przywracanie lustra sprzed kilku miesięcy. Wszystko po to, aby na
maturę system był czysty od różnorakich śmieci. W czasie gdy przywracała nam się ta baza, admin pokazał nam
bardzo fajny program Ranish Partition Manager – dość stary, ale bardzo ciekawy program do modyfikacji
struktury logicznej dysku. Gdy obraz na komputerze się przywrócił zapuściliśmy aktualizację systemu, a później
zaktualizowaliśmy zainstalowane oprogramowanie.

<figure>
  <img src="{static}/images/pw4-ranish.webp" alt="Zrzut ekranu programu Ranish 2.40.00 z 8 lutego 2001 roku. Widoczna przykładowa tabela partycji z dysku o pojemności 12 GB">
  <figcaption>Interfejs Ranisha.</figcaption>
</figure>

## Otwieramy tackę

We wtorek wyciągnęliśmy trzy niesprawne nagrywarki DVD z komputerów w Pracowni z Vistami. Jeden napęd tak się
zablokował, że nie sposób było otworzyć tackę na płyty. Nie pomagały tutaj żadne sztuczki, więc trzeba było
rozebrać urządzenie.

Przyznam szczerze, że moje umiejętności manualne w tym aspekcie nie są za wielkie, więc w pełni oddałem to
zadanie mojemu kompanowi praktykantowi. Jak się okazało niezbędna była lutownica do wylutowania silnika
tacki. Gdy już udało się wszystko odblokować. Podłączyliśmy sprzęt do PC‑ta, ale okazało się, że napęd i tak
jest do bani, bo czyta tylko CD :‑(.

<figure>
  <img src="{static}/images/pw4-cd-kolba.webp" alt="Zdjęcie rozebranego napędu optycznego z widoczną elektroniką, obok czerwona lutownica transformatorowa (pistoletowa)">
  <figcaption>Bez lutownicy się nie obyło.</figcaption>
</figure>

# Czwartek

Trudno właściwie się rozpisywać, bo zajęliśmy się głównie obrazem bazowym w Pracowni 3, ale oprócz tego trzeba
było sprawdzić i skonfigurować kilka rzutników oraz pojawiła się konieczność latania po szkole w celu
odnotowania wszystkiego w ewidencji

## Rzutniki

Na poranną rozgrzewkę dostaliśmy trzy rzutniki do sprawdzenia i skonfigurowania pod egzaminy ustne z
matury. Trzeba było wszystko powłączać, odwrócić obraz (2 z 3 pracowało przykręcone do sufitu), przeczyścić
filtry oraz sprawdzić żywotność lampy. Nikt przecież nie chce jakiejś awarii podczas prezentacji.

Jak się okazało wszystkie urządzenia są w podobnym stopniu zużyte (około 800 godzin świeciła lampa), wszystkie
działają i mają pełny zestaw akcesoriów (tj. pilot i kabel VGA).

## Lustrzanego maratonu ciąg dalszy

Korzystając z dobrodziejstw IPATa w Pracowni 1 zapuściliśmy przywracanie systemu na jednej maszynie. W czasie
gdy się to robiło przeszliśmy do Pracowni 3 wgrać Visual Studio 2008 (takie mamy na stanie) na „komputer do
lustra”, gdyż VS powinien być zainstalowany na egzamin maturalny (abiturient ma prawo wybrać IDE Microsoftu
jako środowisko).

Gdy po kilkunastu minutach wszystko się wgrało, utworzyliśmy od razu obraz Acronisem, tak aby jutro z rana
wejść do pracowni i móc od razu zacząć operację instalacji obrazu na wszystkich PC‑tach w sali.

## Prace nieobowiązkowe

Gdy wszystko zaczęło się dziać automatycznie, poszedłem na śniadanie do kanciapy. Przypomniało mi się, że coś
serwer u nas miał problemy z KDE (stoi na Ubuntu Server 12.04, ale ma doinstalowane środowisko graficzne). Nie
ładował się manager okien, więc załatwiłem to poleceniem `kwin --replace`, a później wyłączyłem też kompozycje
graficzne, gdyż działało to niesamowicie wolno. (Później w `lspci` doczytałem, że grafika działająca na
maszynie to coś z okolicy ATI Radeona 7000 – nie HD7000)

# Piątek

Dzisiaj głównym zadaniem była instalacja systemu z obrazu na wszystkich maszyna w Pracowni 3, a także szał
biurokracji, czyli prostowanie bałaganu w ewidencji.

## Matura, matura

Od samego rana weszliśmy do Pracowni 3, żeby zacząć przywracanie obrazów na komputerach. Zastosowaliśmy tutaj
ten sam trick, co w Pracowni 5 (instalowanej w okolicach poniedziałek-wtorek), czyli skorzystaliśmy z grupy
roboczej.

<figure>
  <img src="{static}/images/pw4-przywracanie-obrazow.webp" alt="Zdjęcie trzech komputerów w pracowni komputerowej, na ekranach program Acronis True Image iprzywracanie obrazów z systemem operacyjnym">
  <figcaption>Masowe przywracanie backupów.</figcaption>
</figure>

Oczywiście to wszystko polegało na klikaniu i czekaniu, ale żeby nie było nudno mieliśmy też inne rzeczy do
roboty.

## Prostowanie bałaganu

Gdy obrazy się przywracały, lataliśmy (głównie mój kolega) po całej szkole szukając zaginionego sprzętu z
Pracowni 1. Okazało się, że niektóre myszki są podmienione, niektóre klawiatury są inne, monitory znajdują się
gdzieś poza salą. Niby nic, żaden problem, jednak pracownia jest współfinansowana ze środków EFS, więc w razie
kontroli wszystko musi się jakby zgadzać. Niestety, okazało się, że w papierach ewidencyjnych panuje mały
bałagan, więc trzeba było wszystko uporządkować, pozaznaczać. W końcu jakiś porządek powinien panować.

Niestety tak się to wszystko dziś wydłużyło, że aż zostaliśmy w pracy trochę ponad planem, no ale nikt nie
chce odpalić fuchy na maturze.

To tyle w tym tygodniu. Nadal podtrzymujemy naszą pozytywną opinię na temat praktyk. Są one dla nas
niesamowitym źródłem wiedzy i doświadczenia w zarządzaniu żywą jednostką, a ponadto dają nam inspirację do
próbowania nowych rzeczy.
