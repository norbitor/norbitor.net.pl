Title: Praktykanci w boju #5
Date: 2013-05-22 20:10
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-5
islug: prwb5

Powoli nasze praktyki zmierzają ku końcowi, jednak jeszcze kilka dobrych dni pracy przed nami. Czas zatem na
piąty odcinek naszej relacji. Tym razem zapinaliśmy wszystko już na ostatni guzik, chociaż też nie obyło się
bez problemów.

# Poniedziałek

Za dwa dni matura z informatyki, a my jeszcze mieliśmy jedną salę do przygotowania. Ponadto trzeba było
ustawić przegródki (tekturowe) pomiędzy stanowiska pracy.

## Tekturki i podpórki

Maturzyści zdający egzamin muszą być od siebie odizolowani tak, aby niemożliwy był jakikolwiek sposób
komunikacji między osobami. Od strony informatycznej polega to na wyłączeniu switcha z prądu - komputery już
wtedy nie mogą się między sobą porozumieć.

Ale to nie wystarczy, trzeba było odgrodzić maszyny tekturką, żeby nie można było zobaczyć ekranu
kolegi/koleżanki obok. Problem tylko w tym, że komputery w Pracowni 3 i 4 nie zapewniały stabilnego oparcia
dla arkusza kartonu. Zabraliśmy więc stare, nieużywane komputery i wykorzystaliśmy je jako stojak. Rozwiązanie
nie jest super eleganckie, ale nikt nie narzekał.

## IPAT, IPAT i... padł

Najwyższy czas przywrócić lustro na wszystkich komputerach w Pracowni 1. Oczywiście chcieliśmy skorzystać z
dobrodziejstw udostępnianych nam przez Intel PAT. Niestety, od samego początku coś nam było nie po drodze. A
to MBR był uznawany za uszkodzony, innym razem partycja. Podejmowaliśmy kilka prób, a jedna skończyła się tak:

<figure>
  <img src="{static}/images/pw5-bluescreen.webp" alt="Zdjęcie kilku komputerów w pracowni komputerowej, widoczne 5 monitorów i jednostek centralnych. Na każdym ekranie BSoD (niebieski ekran śmierci).">
  <figcaption>Masowy Bluescreen :-)</figcaption>
</figure>

Przyznajemy, że zachwalany przez nas Intel PAT zawiódł oczekiwania, ale przyczyny tego stanu rzeczy szukamy w
serwerze. Niestety z ilości zajęć nie udało nam się zrobić screenów z serwera, ale postaramy się to nadrobić w
najbliższym czasie.

Niestety czas naglił, więc nie było czasu aby męczyć się z rozgryzieniem problemów IPAT-a, wzięliśmy
Acronisa - skorzystaliśmy ze sprawdzonego rozwiązania. Przedłużyło się to niesamowicie, tak że jeden z naszych
szefów siedział w pracy do 19:00, no ale na maturze wszystko musi działać.

# Wtorek

Dzionek minął nam na zapinaniu wszystkiego na ostatni guzik, wszak pomiędzy 11, a 12 mieli przyjść maturzyści
w celu sprawdzenia poprawności działania sprzętu komputerowego.

Później jeszcze uruchomiliśmy każde stanowisko, żeby zobaczyć, czy jakiś złośliwy chochlik nie spowodował
awarii. Na szczęście wszystko było w największym porządku. Uruchomiliśmy więc profilaktycznie każdą aplikację,
która może być użyta na egzaminie i zamknęliśmy sprawę „Matura z informatyki 2013”.

Maturzyści przyszli do sal chwilę po 11. Wszystko sprawdzili i, na szczęście, nie zgłosili żadnych uwag do
pracy sprzętu komputerowego. Zmieniliśmy tylko profilaktycznie jedno stanowisko w sali 4, żeby nie mieć cienia
wątpliwości.

## Lepszy serwer

Po godzinie 12, kiedy już zamknęliśmy pracownie na matury można było przynieść do kanciapy nieużywany
serwer. Przyznam szczerze, ciężkie to pudło, szczególnie gdy trzeba było przetransportować go o dwa piętra.

Od razu podłączyliśmy sobie go do prądu, aby zobaczyć czy działa. Skok wydajności w stosunku do tych serwerów,
co działają w Pracowniach 1 i 2 jest niesamowity. Zmieniliśmy tylko hasło dostępu na konto Administratora i
poszliśmy do domu.

# Środa

Nastał dzień, który napsuł nam krwi w ostatnich dniach, ale jednocześnie dzień wielkiej ulgi. Matura z
Informatyki poszła w ruch abiturientów, my natomiast zajęliśmy się przyniesionym serwerem oraz nadzorem nad
maturzystami, którzy przyszli dziś sprawdzić swoje prezentacje.

## Zabawa z serwerem

Na początek przyjrzeliśmy się zarządzaniem użytkowników w systemie Windows Server 2003 for Small Business
Server. Usunęliśmy starych uczniów, którzy już się u nas nie uczą oraz przenieśliśmy niektórych do
odpowiednich klas.

Później poszedłem do Pracowni 2 po skrypt tworzący automatycznie konta nowych użytkowników, gdyż brakowało nam
uczniów z klasy pierwszej. Zapuściliśmy tworzenie użytkowników (trochę to trwało), tymczasem przyszło kolejne
zadanie.

## Sprawdzanie prezentacji

W czasie, gdy nasi admini stanowili nadzór techniczny w pracowniach informatycznych, my sprawdzaliśmy z
chętnymi maturzystami poprawność działania ich prezentacji maturalnych. Na szczęście w większości wszystko
działało poprawnie, ale okazało się, że trzeba gdzieniegdzie dograć kodeki, więc kolega wgrał te kodeki.

## Siadł HP-ek

Przychodzimy do Pracowni 5, bo o coś poprosiła nas jedna nauczycielka, jednak rzuca nam się w oczy co innego –
Blue Screen na jednym stanowisku. Wygaszacz? Nie. Bez chwili zastanowienia odpalamy Acronisa i przywracamy
obraz, jednak problem nie ustąpił. System się ładuje, ładuje i ulubieniec wszystkich informatyków – Niebieski
Ekran. Wzięliśmy sprzęt do kanciapy i zajmiemy się nim jutro.

To tyle na dziś. Przepraszamy za mniejszą ilość zdjęć, ale jakoś powylatywało nam to z głowy. Kolejny odcinek
już w piątek.
