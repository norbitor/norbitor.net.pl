Title: Praktykanci w boju #6
Date: 2013-05-28 00:00
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-6
islug: prwb6

Zapraszamy na szósty odcinek naszego cyklu o przebiegu praktyk. Na wstępie przepraszamy, że odcinek ukazał się
dopiero dziś, a nie jak obiecywaliśmy w piątek. Niestety, natłok zajęć spowodował opóźnienie. W ramach
rekompensaty opisujemy jeden dzień więcej.

Dzisiaj o wdrożeniu innego serwera w Pracowni 2, powrocie pracowni maturalnych do zwykłych zajęć, ciąg dalszy
ewidencjonowania i zabawa z serwerami.

# Czwartek

Matura z Informatyki już za nami, trzeba zatem przystosować pracownie do zwykłych zajęć. Zaczęliśmy też już
powoli przygotowywać przyniesiony serwer do pracy w pracowni 2, zajęliśmy się sprawą komputera HP z Pracowni
5, który odmówił posłuszeństwa.

## Po maturach

Matura z Informatyki się skończyła, zatem do sal komputerowych wróciły normalne lekcje. W Pracowni 1 oznaczało
to potrzebę usunięcia kont użytkowników na maturę oraz podpięcie PC‑tów pod domenę.

Podzieliliśmy się, więc w miarę sprawnie to poszło. Później padła propozycja, aby przyjrzeć się bliżej
switchowi D‑Linka DES‑3052, gdyż jest to sprzęt zarządzalny. Jednak domyślnie konfigurację można zmieniać
jedynie z poziomu portu COM, więc przynieśliśmy potrzebny nam przewód RS‑232. Korzystając z „Bray's Terminal”
ustawiliśmy switchowi odpowiedni adres IP i maskę podsieci. Później weszliśmy sobie na panel administracyjny,
który prezentuje się tak:

<figure>
  <img src="{static}/images/pw6-switch-gui.webp" alt="Zrzut ekranu przedstawiający główne okno panelu zarządzenia switchem D-Link. Widoczny schemat gniazdek urządzenia oraz tabelka z postawowymi informacjami o urządzeniu">
  <figcaption>Okno główne zarządzania switchem.</figcaption>
</figure>

Bawić w ustawieniach będziemy w wolnym czasie, obecnie nie jest to nasz priorytet.

## Adaptacja przyniesionego serwera

Okazało się, że skrypt tworzący konta nie przypisuje ich do konkretnych klas (chodzi o zachowanie porządku,
domyślnie system wstawia wszystkich do jednej jednostki organizacyjnej), więc zamiast szukać każdego ucznia po
kolei i przypisywać ich do klas, szybciej było skasować utworzone konta (nie było ich za wiele) i zacząć
tworzyć nowe. W międzyczasie zapuszczaliśmy tworzenie kolejnych kont i gdy dana klasa się utworzyła,
przypisywaliśmy uczniów do odpowiedniego im miejsca.

Z racji innych zajęć skrypt utworzył tylko 60 kont, ale jutro myślę, że dokończymy tą operację i w końcu
będzie można wstawić ten serwer do pracowni i uprzyjemnić trochę pracę wszystkim.

## Inne zajęcia

Okazało się, że jeden komputer w Pracowni 1 nie uzyskuje adresu IP (dostaje domyślny). Zaczęliśmy od
sprawdzenia przewodu. Przepięliśmy kabel z innego komputera i wszystko zaczęło działać normalnie. Później
podejrzewany kabel wpięliśmy w drugie gniazdko i też wszystko dobrze. Na koniec powpinaliśmy wszystko z
powrotem i, co najlepsze, sieć w Pececie nadal działa poprawnie. Oczywiście to nie jest rozwiązanie problemu i
trzeba będzie się temu przyjrzeć dokładniej.

Później kolega praktykant zajął się komputerem z Pracowni 5, o którym wspominałem w poprzednim
odcinku. Okazało się, że owszem, obraz przywróciliśmy, jednak nie zwróciliśmy uwagi, gdzie się przywróci i
tak, mieliśmy dwie partycje z siódemką na dysku. Wrzucił jeszcze raz przywracanie obrazu (wiem, że nie trzeba
było, ale musieliśmy wychodzić, więc najprościej było zrobić fallback kompletny) i jutro zobaczymy efekty.

# Piątek

Wszystkie konta na przyniesionym serwerze się potworzyły, więc można było wstawić go do Pracowni 2. Ponadto
latania z ewidencją ciąg dalszy oraz przypadkowa utrata danych (na szczęcie mało ważnych i zbackupowanych) na
starym serwerze w Pracowni 2.

## Konta stworzone

Pierwszą rzeczą po przyjściu jaką zrobiłem to odpalenie serwera i dokończenie tworzenia kont. Przy okazji
dowiedziałem się, że można odpalić kilka skryptów na raz i będzie to bez problemu działać. Oczywiście
zapuściłem tworzenie kont i poszedłem pomóc koledze i adminowi w spisywaniu ewidencji.

Cóż, spodziewamy się kontroli sprzętu współfinansowanego z EFS‑u w latach 2004-2006, więc praktycznie cały
dzień lataliśmy (głównie współpraktykant) po szkole odznaczając każde urządzenie w pracowni na liście. Ot,
praca taka praca w budżetówce.

<figure>
  <img src="{static}/images/pw6-efs.webp" alt="Plansza z logami EFS oraz UE, poniżej napis „Zakup współfinansowany ze środków Unii Europejskiej w ramach Europejskiego Funduszu Społecznego">
  <figcaption>Źródło szczęścia i problemów.</figcaption>
</figure>

## Zmiana serwera

Gdy drugi admin rozpoczął zmianę, poszliśmy do Pracowni 2 przygotować sobie jeden dysk do włożenia do serwera
w Pracowni 1 (chcieliśmy zrobić RAID-0). Ładnie sformatowaliśmy sobie partycję na tym dysku i nagle coś nas
tknęło, żeby wyciągnąć sformatowany dysk z serwera – podczas pracy.

Nie był to dobry pomysł. Chwilkę później kombinacją miłych słów i kodów błędu przywitał nas BSoD. Okazało się,
że wyciągnęliśmy dysk systemowy, co spowodowało awarię systemu. Z braku czasu, zerknęliśmy na serwer, na
którym właśnie skończyły się tworzyć konta. Decyzja była oczywista – zamianka.

Szybciutko przetransportowaliśmy dość sporych rozmiarów puchę i uruchomiliśmy. Przywitał nas działający
sprawnie Windows Server 2003 for SBS. Zmieniliśmy tylko domenę na komputerach klienckich i zadowoleni z
trzeciego tygodnia praktyk poszliśmy do domu.

# Poniedziałek

Ostatnie trzy dni praktyk. Nie siedzimy jednak z tego powodu w kanciapie czekając na koniec, tylko nadal pełni
chęci pracujemy. Dzisiaj klon dysku, niszczymy system na serwerze oraz bawię się Sambą na Ubuntu 12.04.

## Klonujemy partycję

Mając wolny dysk twardy, postanowiliśmy sklonować zawartość wolno pracującego dysku na serwerze w
Pracowni 1. Zapuściliśmy więc program i poszliśmy robić inne rzeczy. W końcu taka operacja miała zająć około
dwóch godzin.

Niestety, po tym czasie zostaliśmy rozczarowani. Obstawiany przez nas dysk twardy okazał się sprawny, a
problem leży zakopany gdzieś mocno w systemie Windows. Uruchomiliśmy system z „nowego” dysku i przywitały nas
te same wspaniałe transfery na poziomie 1,0 MB/s. Tragedia. Nie mamy już co się zastanawiać, po prostu czas
postawić system na nowo. Na szczęście zachowujemy tutaj status quo – wszystko działa jak działało.

## Psujemy

Weszliśmy do Pracowni 2, żeby poznać inny sposób zdalnej instalacji systemu Windows, tj. RIS. Niestety,
okazało się, że obrazy Windowsa XP, które są zainstalowane na serwerze nie współgrają z komputerami w
sali. Faktem jest, że urządzenie jest z innej pracowni, ale nie sądziliśmy, że spotka nas komunikat o braku
sterowników do karty sieciowej.

Tutaj też nasz admin zaproponował postawienie serwera na nowo. Zgodziliśmy się. Wrzuciliśmy płytkę
instalacyjną Windows Server 2003 for SBS dla MEN - dla i dla :‑). Wszystko szło nawet dobrze, ale tylko do
pierwszego restartu. Przywitał nas nasz ulubieniec – Niebieski Ekran Śmierci. Pomyślałem sobie – sterowniki
SCSI. Korzystając z faktu, że serwer posiada stację dyskietek pobrałem sterowniki do kontrolera z nadzieją, że
to rozwiąże niebieski problem.

Moja nadzieja okazała się tym razem złudna, nic to nie dało, nic nie pomogło. Stworzyliśmy zatem na szybcika
konta lokalne, tak aby zajęcia mogły się odbywać i z racji, że czas pracy minął zostaliśmy oddelegowani do
swoich pulpitów.

## Samba na Ubuntu

W czasie, gdy wszystko się niby instalowało, przysiadłem do serwera z Ubuntu, gdyż Samba coś nie chciała nam
udostępniać zasobów w sieci z partycji innych niż główna. Chwilka główkowania i przyszedł mi do głowy pomysł,
że dodam dodatkowe partycje do /etc/fstab (cały czas były montowane w czasie pracy użytkownika). Szybka edycja
pliku edytorem tekstu, restart i wszystko śmiga. Chociaż coś dzisiaj :‑).

To tyle w tym odcinku. Następny będzie już ostatnim, gdyż środa wieńczy nasz 18‑sto dniowy okres
pracy. Dziękujemy za uwagę i zapraszamy na ostatnią część na przełomie środy i czwartku.
