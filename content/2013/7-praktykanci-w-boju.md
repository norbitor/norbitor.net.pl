Title: Praktykanci w boju #7 – ostatni
Date: 2013-05-30 14:18
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-7
islug: prwb7

Nasze praktyki niestety dobiegły końca. Czas więc powrócić do szarej, szkolnej rzeczywistości. Mimo to przez
ostatnie dwa dni pracy wciąż nie próżnowaliśmy, a pracy było naprawdę sporo. Dzisiaj o instalacji Windows
Server 2003 ze skryptu przygotowanego specjalnie dla MEN‑u oraz o zamieszaniu związanym z kontrolą sprzętu z
EFS‑u.

# Wtorek

Dzisiaj rozpoczęliśmy instalację serwerów, zanieśliśmy obiecany od dawna komputer do studia TV oraz
oklejaliśmy sprzęt naklejkami informującymi o współfinansowaniu ze środków UE.

## Windows Server 2003 dla MEN

Bardzo chcieliśmy skonfigurować RAID (na początku 0, później już myślałem nad 1) na serwerach. Był tylko jeden
problem. Płytki instalacyjne ni jak chciały macierz obsługiwać. Problem leżał w braku sterowników trybu
tekstowego, których nie dało się w żaden sposób przetransportować.

Powiecie nLite. Jednak problem w tym, że te płytki są fajnie oskryptowane przez giganta z Redmond i ingerencja
nLitem powoduje zawalenie się skryptu i wywalenie się instalatora. Z racji tego, że to już przed ostatni dzień
praktyk, a pora była już dość późna, zrezygnowaliśmy z RAID-a, jednak nie jesteśmy z tego powodu zadowoleni i
na pewno kiedyś wyczaimy jak to zrobić.

Puściliśmy zwykłą instalację, która bez problemów ruszyła z kopyta.

<figure>
  <img src="{static}/images/pw7-winserver2003.webp" alt="Zrzut ekranu instalacyjnego Windows Server 2003, dominuje kolor szary">
  <figcaption>Widok, na który czekaliśmy.</figcaption>
</figure>

## Naklejki, naklejki

O godzinie 11:00 dowiedzieliśmy się, że kontrola z EFS‑u będzie jutro. Zaczęliśmy latać jak szaleni do
pracowni z naklejkami informującymi o współfinansowaniu (oczywiście tam, gdzie takie współfinansowanie
występowało). Oczywiście zadanie to z IT jest związane tylko tym, że znakowaliśmy sprzęt komputerowy, jednak
było to konieczne, żeby było jak najmniej możliwości doczepienia się ze strony kontrolerów :‑).

Dodatkową sprawą było zaniesienie komputera do studia TV. Przynajmniej teraz mają niezłą maszynkę do cyfrowego
montażu wideo.

# Środa

Ostatni dzień praktyk. Instalacji serwera ciąg dalszy, czekanie na kontrolę i czas na podziękowania oraz
podsumowania. Serwery się instalują

Skrypty działają, systemy się instalują. Proces fajny, bo automatyczny i polega tylko na dostarczaniu płytek
instalatorowi systemu. Szkoda tylko, że trwa to już drugi dzień, ale nie można narzekać, przynajmniej będzie
to trochę lepiej chodziło. Zobaczymy po weekendzie końcowo-majowym.

## Ostatnie przygotowania przed kontrolą

Od rana dopinaliśmy wszystko na ostatni guzik. Drukowaliśmy kartki z informacjami o współfinansowaniu,
laminowaliśmy, przyklejaliśmy. Ponadto nadal szukaliśmy brakującego sprzętu. Kosztowało nas to wiele nerwów i
pokazało jaki burdel panuje w papierach. Chwilkę przed czasem zebraliśmy tylko sprzęt z sal, gdzie odbywała
się ustna matura i czekaliśmy na godzinę zero, tj. 11.

Nadeszła w końcu oczekiwana godzina. Przyszła kontrola, przeszła się po salach. I… nic. Wszystko w porządku,
więc mogliśmy odetchnąć z ulgą. Później nadszedł czas na podziękowania i skończyliśmy w ten sposób nasze
praktyki zawodowe.

# Podsumowanie

Niewątpliwie maj był dla nas niesamowicie ważnym miesiącem. Miesiącem pracy w zawodzie, który jest naszą pasją
i mimo, że popełnialiśmy nie raz błędy mniejsze lub większe, to jednak bardzo wiele się nauczyliśmy. Ważną
rolę odegrał też blog, gdzie Wasze opinie i podpowiedzi pozwalają nam być coraz lepszymi.

<figure>
  <img src="{static}/images/pw7-error.webp" alt="Okienko z komunikatem o błędzie „An error has occured while creating an error report”, ikonka czerwonego X">
  <figcaption>Czasem brakło nam logiki w rozwiązywaniu problemów.</figcaption>
</figure>

Obaj twierdzimy, że miesiąc to zdecydowanie za mało i czujemy niedosyt. Szkoda, że nie udało nam się
zrealizować wszystkich naszych planów i założeń, ale i tak twierdzimy, że udało się nam dość dużo, a to co nam
się nie wyszło, dało nam sporo do myślenia i powód do dalszego poszerzania wiedzy we własnym zakresie.

Dziękujemy za ciepłe przyjęcie naszej serii, wszelkie głosy pochwały, jak i krytyki oraz chęć pomocy i
rozwiązywania niektórych problemów.
