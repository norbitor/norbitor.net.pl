Title: Praktykanci w boju #2
Date: 2013-05-09 20:42
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-2
islug: prwb2

# Wtorek

Drugi dzień praktyk – maturzyści zmagali się ze Stefanem Żeromskim i Elizą Orzeszkową, my tymczasem
pracowaliśmy. Po pozytywnych wrażeniach z dnia pierwszego z chęcią ruszyliśmy do pracy. Rzeczy do zrobienia
było kilka. Najprostszą było po prostu dokończenie migracji danych do komputera dla studia TV.

## Sprawdzenie nowych komputerów

Jak się okazało, przy przygotowywaniu komputerów na maturę jedna maszyna odmówiła posłuszeństwa, chociaż
dzielnie Windows 8 uruchamiał opcję automatycznej naprawy, nie przynosiło to rezultatu. Dostaliśmy więc
zadanie, aby rozpakować resztę sprzętu i posprawdzać, czy wszystko tam jakoś działa. Rozpakowaliśmy kartony ze
sprzętem, ustawiliśmy go w wolnej sali i uruchamialiśmy. Na szczęście wszystko śmigało jak trzeba.

W międzyczasie poszedłem też do szkolnej pielęgniarki, która miała problem z paskiem powiadomień w
Androidzie. Rozwiązanie problemu okazało się banalne – restart urządzenia i wszystko wróciło do normy. Wiem,
że to nie jest likwidacja przyczyny, ale tutaj już Samsung musi wypuścić odpowiednią łatę.

## Aktywacja Visty i wymiana napędu

Jeden z naszych adminów przywrócił obraz systemu na komputerze, który tego wymagał. Niestety koniecznością
okazała się aktywacja telefoniczna, którą zajął się mój kolega. Jak twierdzi proces ten jest upierdliwy i
czasochłonny, ale jak mus to mus. Ja tymczasem dostałem inne zadanie. Musiałem sprawdzić działanie napędu DVD
na jednym komputerze – okazało się, że był niesprawny, więc konieczna była wymiana, czyli pójście do magazynu,
wyczajenie podobnego sprzętu (LG na IDE), zamiana i zapis w inwentarzu.

## Kopia aktywacji i wymiana kabli w pracowni

Kolejną sprawą, którą trzeba było się zająć to kopia zapasowa plików aktywacji w pracowni z Vistami, żeby się
już potem z dzwonieniem do Microsoftu nie trudzić. Czynność monotonna, wykonana pewnym fajnym sofcikiem na
każdej maszynie, a że wymagał doinstalowania .NET Frameworka 4, który w systemach nie był obecny,
skorzystaliśmy z dobrodziejstwa serwera - udostępniania plików i zapuściliśmy instalację prawie jednocześnie,
więc szybko to w miarę poszło.

Kiedy zacząłem powoli kończyć backup plików aktywacji, kolega poszedł zrobić inwenturę kabli ethernetowych w
drugiej pracowni i jak się okazało, większość okablowania albo była powpinana gdziekolwiek, albo były
poułamywane plastiki w RJ‑kach, więc trzeba będzie je wymienić.

# Środa

Trzeci dzień praktyk, abiturienci zmagali się dziś z Królową Nauk, my tymczasem mieliśmy trochę luźniejszy
dzień pracy.

## Poznajemy Windows Server

Zaczęliśmy od rozmowy z administratorem, aby pokazał nam trochę operowanie na domenie używającej Active
Directory. Dowiedzieliśmy się jak mniej więcej zarządzać użytkownikami oraz komputerami. Zobaczyliśmy także
jak podpiąć nowy komputer do domeny i nadać mu odpowiedni poziom uprawnień. Osobiście bardzo mi się to
spodobało, gdyż można sobie użytkowników ładnie podzielić na grupy i nadać odpowiednie uprawnienia, np.:
zablokować możliwość zmiany tapety (było to konieczne, bo uczniowie ustawiali sobie tapety z trzema parabolami
o ramionach skierowanych w dół). Jasne, nic odkrywczego, można to przecież zmienić w gpedit, jednak tutaj
efekt jest globalny i moim zdaniem bardziej intuicyjny.

Niestety, nasze serwery chodzą niesamowicie wolno, więc skorzystaliśmy z płytki rescue przygotowanej przez
mojego kolegę – ot, zwykły miks różnych darmowych programów antywirusowych i linuksów, tak żeby mieć wszystko
ładnie na jednym pendrive/DVD, a nie latać z płytkami. Jak się okazało jeden serwer miał problem z uzyskałem
połączenia z siecią na płytce ratunkowej, ale tu z pomocą przyszła moja znajomość uniksa i po 2‑óch minutach
kombinowania uzyskałem dostęp do sieci. Puściliśmy skan „na noc” i zajęliśmy się innymi sprawami.

## Instalacja sterowników tablicy interaktywnej

Antywirusy chodziły, więc żeby się nie nudzić admin dał nam zadanie – zainstalujcie i skonfigurujcie tablicę
interaktywną na komputerze nauczyciela w jednej sali komputerowej. Dostałem płytki i wziąłem się z kolegą do
roboty. Sterowniki zainstalowały się gładko. Tak przynajmniej myśleliśmy. Rzeczywistość okazała się bardziej
brutalna. Wskaźnik nie miał żadnej ochoty współpracować z interfejsem Bluetooth i sterownikiem tablicy – niby
wykrywa, niby uznaje za podłączony, ale nijak ma się to do chęci pracy.

Stwierdziłem, że najlepszym pomysłem będzie pobranie sterowników do tablicy z Internetu. Przeszukiwałem stronę
producenta – PolyVision, jednak dopiero po 15 minutach gmerania po stronach suportu – jest sterownik,
pobieram. No cóż, prędkość Internetu w naszej szkole nie powala na kolana (Speedtest wskazuje 20Mbit/28Mbit
jednak rzeczywistość jest smutniejsza – średnio 100‑200 kB/s downloadu), więc odpaliłem telewizor, który jest
w sali i oglądałem Discovery oraz zapuściłem instalację aktualizacji z Windows Update na przywróconym niedawno
z obrazu systemie. Później tylko instalacja i w końcu jest! Działa. Szybko skalibrowałem urządzenie i trochę
się pobawiłem.

# Czwartek

Ostatni dzień spokoju w szkole. Jutro już z powrotem zaczynają się normalne zajęcia. My tymczasem
nadal walczymy z serwerami. Wziąłem także moją RaspberryPi, żeby zobaczyć jak działa stara karta
Pinnacle’a. Myślałem, że złącza analogowe na śledziu służą do podłączenia urządzenia wejściowego.

## Bitwa o serwery

Antywirus znalazł na obu serwerach nieco syfu, jednak usunięcie tego niewiele dało. Stwierdziliśmy, że pewnie
coś złego stało się ze sterownikami do dysku/kontrolera. W ruch poszły różne Driver Packi, ale one wiele nie
dały. Nie podejrzewamy awarii dysku twardego, gdyż z płyty ratunkowej transfer mierzony programem HDTune jest
w porządku - około 70MB/s, a gdy działa Windows Server 2003 for Small Business Server spada do 0.5‑4 MB/s,
czyli do wartości nie do zaakceptowania, co odbija się na szybkości ładowania kont na komputerach wpiętych w
domenę. Cały czas kombinujemy jak mu pomóc, ale pewnie skończymy na formacie, który jest ostatecznością, jeśli
ktoś spotkał się z takim zjawiskiem prosimy o sugestię w komentarzach.

## Streaming i inne

Dostałem zadanie, aby wykombinować jakieś oprogramowanie do streamowania wideo w sieci lokalnej do komputera
dla studia TV, aby móc cyfrowo nadawać audycje okolicznościowe (szkoła nie ma miejsca na aulę, więc w ruchu
jest wewnętrzna telewizja – obecnie działa na zasadzie analogowej TV Kablowej, ale widocznie planowane jest
przejście na cyfrowe nadawanie po sieci). Wybór padł na VLC, bo jakoś nigdy nie sprawiał mi z tym
problemów. Wstępnie zobaczyłem co się da zrobić i udało się coś wystreamować. Co prawda tylko w podsieci WiFi,
ale wiemy, że działa. Ponadto ogarniałem zasadę działania karty Pinnacle’a do przechwytu. Okazało się, że
wbrew moim przypuszczeniom karta ta potrafi przechwytywać z kamer DV, a złącza cinch i S‑Video są tylko
wyjściem gotowego materiału na magnetowid, także moja RaspberryPi nie wyświetliła dziś nic.

## Aktualizacja oprogramowania i uszkodzony zasilacz

Ponadto trzeba było zaktualizować oprogramowanie na jednym komputerze, który będzie bazą dla obrazu dysku -
bazy do przywracania systemów w pracowni. Ściągając potrzebnego Eclipse’a zauważyłem ciekawą rzecz. Transfer
był na poziomie 170 kB/s. W międzyczasie włączyła się aktualizacja Firefoksa, która pobierała się z podobną
prędkością. Z ciekawości mój kolega wgrał sobie wersję próbną Internet Download Managera na drugi komputer i
zapuścił pobieranie przykładowego linuksa – transfer zwiększył się do około 36 Mbit/s. Wniosek: okno na świat
w głównej serwerowni ma ustawione wąskie gardło na pojedyncze połączenie.

Jak się okazało komputer w jednej pracowni nie chciał się uruchomić. Zadania tego podjął się mój kolega. Wziął
komputer do kanciapy i włączył tymczasowo zapasowy zasilacz. Ruszyło wszystko z kopyta. Nie zamontował go
jednak na stałe, gdyż ten komputer to korporacyjny DELL Vostro 260s, a on posiada zasilacz o niestandardowych
wymiarach, więc zwykły ATX po prostu nie zmieściłby się w obudowie.

Jak widać, czwartek był dniem bardziej luźniejszym i zapchanym przez czynności, które po prostu jadły dużo
czasu. Oboje czekamy teraz na ukończenie bazowego obrazu dysku dla nowych HP‑ków, aby w końcu móc je
przygotować do podmianki w pracowni z tzw. „trupami”

To tyle w tym odcinku (przepraszam za brak ilustracji, ale te wkrótce się pojawią).
