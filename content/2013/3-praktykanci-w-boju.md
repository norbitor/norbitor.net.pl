Title: Praktykanci w boju #3
Date: 2013-05-14 23:36
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-3
islug: prwb3

Zapraszamy na trzeci odcinek cyklu. Dziś przeczytacie o: pomysłach na wewnątrzszkolne VoD, planach przebudowy
sieci, wymianie pracowni i kilku innych sprawach.

Na początek jednak krótka charakterystyka pracowni komputerowych, którą powinniśmy zrobić już w pierwszym
odcinku (numery są umowne):

* Pracownia 1 – 16 (15 + Nauczyciel) komputerów z Windows Vista Business działające na domenie z serwerem HP i systemem Windows Server 2003 for SBS. Pracownia korzysta z Intel Platform Administration Technology, każdy komputer ma swój klucz licencyjny.
* Pracownia 2 – 16 komputerów z Windows XP Professional działające na domenie z serwerem HP i systemem Windows Server 2003 for SBS.
* Pracownia 3 – 17 komputerów Dell Vostro 260s z systemem Windows 7 Professional. Pracownia nie posiada serwera, skonfigurowana zwykła grupa robocza.
* Pracownia 4 – 17 komputerów HP z systemem Windows XP Professional. Pracownia nie posiada serwera, skonfigurowana zwykła grupa robocza.
* Pracownia 5 – 16 komputerów HP Compaq Elite 8300. Pracownia nie posiada serwera. Grupa robocza skonfigurowana ze względu na instalacje obrazów.
* Pracownia 6 – 16 komputerów z systemem Windows XP Professional. Pracownia posiada zapasowy serwer z systemem Windows Server 2003 for SBS.

# Piątek

Uczniowie wrócili dziś na wąskie korytarze szkoły, jednak i tak było luźniej niż zwykle. My tymczasem dziś
dużo kombinowaliśmy.

## Szkolne VoD, streaming, etc

W czasie gdy mój kolega badał serwery, ja zająłem się researchem na temat tego, jak wykombinować szkolną bazę
VoD oraz umożliwić streamowanie. Trochę poczytałem, pokombinowałem, zrobiłem próbną transmisję na UStream.tv,
ale na razie nic wielkiego nie wynikło. Zobaczymy co będzie później.

Tymczasem screen zabójczych prędkości HDD na serwerze w Pracowni 2 (w Pracowni 1 jest podobnie). Dodam tylko,
że kanały DMA ustawione są na sztywno, więc propozycja podana przez @jakistamnick nie zadziałała. Mimo to,
dziękujemy za chęć pomocy.

<figure>
  <img src="{static}/images/pw3-hdtune.webp" alt="Zrzut ekranu programu HDTune przedstawiający prędkość dysku średnio 2,5 MB/s i czasem dostępu 34,9 ms.">
  <figcaption>„Szybkość” dysku.</figcaption>
</figure>

## Plany modernizacji sieci

Od jakiegoś czasu w naszej kanciapie poruszany jest temat lekkiej modernizacji szkolnej sieci
komputerowej. Przede wszystkim chodzi o rozpowszechnienie sygnału Wi‑Fi po całym budynki oraz uproszczenie
całej struktury. To będzie raczej temat na rozważania w przyszłym tygodniu.

Piątek wieńczy pierwszy tydzień pracy jako praktykanci. Wrażenia są dopóki co bardzo pozytywne i raczej nie
możemy się doczekać poniedziałku :‑)

# Poniedziałek

Drugi tydzień praktyk, które są dla nas bardzo fajnym przeżyciem. Dzień ten minął na wymianie zasilacza w
jednym Dellu Vostro oraz budowie prowizorycznej sieci komputerowej.

## Wymiana zasilacza

Z rana zawitał do szkoły kurier, przywożąc nam nowy zasilacz do zepsutego Della z Pracowni 3. Dostaliśmy więc
zadanie wymiany tegoż PSU. Śrubokręt w dłoń i robimy – nic trudnego w końcu. Swoją drogą bardzo zmyślnie
przeprowadzane są przewody w tych PC‑tach. Zamieszczam fotkę tego komputera „od środka”

<figure>
  <img src="{static}/images/pw3-dell-w-srodku.webp" alt="Zdjęcie komputera Dell Vostro typu desktop z otwartą górną pokrywą.">
  <figcaption>Dell Vostro od środka.</figcaption>
</figure>

## Test napędów i inne zajęcia

Dostałem polecenie sprawdzenia poprawnego działania napędów optycznych w Pracowni 1 – muszą być sprawne na
Maturę i Egzamin Zawodowy. Przywitał mnie jednak wywalony bezpiecznik w skrzynce na korytarzu. Zaczęło się
trochę biegania, bo trzeba było zejść do portierni (z 4‑tego piętra na parter) po klucz do szafki elektrycznej
oraz później go oddać. No cóż, hebel podniesiony, wszystko jakoś działa, więc do dzieła.

Wziąłem zatem po płytce CD i DVD i po kolei sprawdzałem. Jak się okazało 3 sztuki były niesprawne, w tym jeden
DVD‑ROM nie chciał wysuwać tacki. Chciałem tego samego dnia wyhaczyć jakieś napędy, ale zabrakło na to czasu.

W międzyczasie kolega mój szukał przejściówki z Minijacka na Jacka, gdy już ją znalazł pomagał mi w tej
żmudnej dość pracy. Później dostaliśmy jeszcze jedno zadanie sprawdzenia napędów, tym razem w Pracowni 3.

## Budowa prowizorycznej sieci

Obrazy gotowe, więc można już budować nową pracownię nr 5. Przynieśliśmy komputery z obrazem dysku oraz switch
do „sali operacyjnej”. Podpinamy kabelki i zaczynamy zabawę. Na jednym komputerze włączamy udostępnianie
plików, drugi z pomocą Acronisa ściąga sobie ten obraz i instaluje go na dysku lokalnym.

Zadziwiło mnie to, że adresacja IP robiona automatycznie przez Windowsa 169.254.x.x pozwala na zbudowanie
prowizorycznej sieci (komputery „się widzą” i nie ma potrzeby robienia żadnej statycznej adresacji
IP). Później dopiero wyczytałem, że od tego jest usługa APIPA.

Puszczamy przywracanie, no ale to tak wolno coś idzie. 50‑60 Mbit/s. Tutaj nam jeden z adminów mówi, że w
Pracowni 1 jest switch z gniazdami Gigabit Ethernet. Nie było chwili zastanowienia, wymieniamy
przełącznik. Niestety, okazało się, że urządzenie ma porty 1Gb/s, ale tylko na czterech złączach. Lepsze
jednak to niż nic, przynajmniej dwa komputery szybciej się przywrócą.

Tutaj też przekonaliśmy się jakie znaczenie ma dobry kabel Ethernet - jednego nie mogliśmy za żadne skarby
zmusić do działania na gigabicie. Inny kabel rozwiązał sprawę. Szkoda tylko, że z 1Gb/s uzyskaliśmy w porywach
300 Mb/s, ale lepsze to niż nic. System przywracał się i tak średnio 20 minut, a nie godzinę. Dodam tylko, że
kopiowanie obrazów z jednej maszyny na więcej niż jeden komputer jednocześnie to raczej zły pomysł - czas
operacji nieproporcjonalnie się wydłuża. Poniżej fotka przedstawiająca transfer.

<figure>
  <img src="{static}/images/pw3-transfer-gigeth.webp" alt="Zdjęcie ekranu z widgetem Network Monitor pokazujący transfer 226,7 Mb/s">
  <figcaption>Transfer na Gigabit Ethernet.</figcaption>
</figure>

Tak oto minął nam drugi poniedziałek w pracy. Nie przywróciliśmy wszystkich komputerów, ale mieliśmy już bazę
na wtorek.

# Wtorek

Dzień ten był dość zapracowany. Przywrócenie obrazów do końca, demontaż starych komputerów z pracowni 5,
instalacja właśnie zrobionych oraz wymiana trzech napędów w pracowni 1.

## Kończymy obrazy

Wchodzimy do pomieszczenia, patrzę tak sobie na switch i mówię, mini plątanina, będzie fotka :‑).

<figure>
  <img src="{static}/images/pw3-prowizoryczna-siec.webp" alt="Zdjęcie switcha ethernetowego z podłączonym tuzinem kabli UTP 5e">
  <figcaption>Bardzo mała plątaninka.</figcaption>
</figure>

Oczywiście obsługa Acronisa to żadna filozofia. Klikasz i się robi, zatem zapuściliśmy kilka maszyn aby się
przywracało i w międzyczasie poszliśmy obok do pracowni 5 zacząć demontaż starych PC‑tów. Przy okazji admin
powiedział nam, że możemy z tych komputerów powyciągać napędy, które były nam potrzebne, co też (po
sprawdzeniu działania) uczyniliśmy.

Po przerwie śniadaniowej obrazy się ładnie przywróciły, więc zaczęliśmy migrację komputerów. Żeby za dużo nie
dźwigać wykorzystaliśmy moc krzeseł biurowych (i ich kółek). Chwila moment, podpinamy. Wszystko działa, no
prawie. Dwa komputery coś sieci nie chcą zobaczyć (ani kabla). Okazało się, że przestawiliśmy kartę sieciową
na sztywno na 1Gb/s – a już chcieliśmy gniazda wymieniać. Wygrało jednak doświadczenie drugiego admina, który
sprawdził jeszcze kabel z innego PC i też nie działało. Szybka zmiana w sterowniku i wszystko już
pracuje. Sprawa zamknięta.

## Splittery i napędy

Po krótkiej przerwie dostaliśmy zadanie wydobycia z magazynu trzech splitterów VGA (i zasilaczy do nich) oraz
sprawdzenie działania. Zwykłe wpinanie kabelków i patrzenie na monitory. Niestety, jedno urządzenie okazało
się wadliwe. No to wybieramy losowe inne i działa, koniec problemów.

Czas zająć się napędami. Idziemy do Pracowni 1 korzystając z tego, że nie było wtedy zajęć. Wymieniamy i
pojawił się mały problem. Jeden komputer za Chiny nie chciał odpalić. Jak się okazało coś już siedzi w
zasilaczu i trzeba chwilkę poczekać – wtedy odpali.

# Podsumowanie

To już koniec trzeciego odcinka cyklu. Jak widzicie, nie próżnujemy i dobrze. Zobaczymy co będzie na nas
czekać w kolejnych dniach.
