Title: Praktykanci w boju #1
Date: 2013-05-06 20:20
Modified: 2025-02-04 14:58
Category: Praktykanci w boju
Tags: praktyki zawodowe, informatyka, technikum, sysadmin
Slug: praktykanci-w-boju-1
islug: prwb1

Cześć! Zgodnie z wcześniejszą zapowiedzią, oto jest, pierwszy odcinek cyklu wpisów o moich i mojego kolegi
perypetiach na praktykach zawodowych. Seria wpisów "Praktykanci w boju" została pierwotnie opublikowana w 2013
roku na łamach blogów vortalu dobreprogramy.pl, lecz postanowiłem je przenieść tutaj, aby mieć kopię, na
wypadek, gdyby kiedyś skasowano część blogową lub cały vortal (jak to się stało z PCLabem w 2020 roku).

Poza pierwszym i ostatnim akapitem tego wpisu, pozostała treść całej serii jest oryginalna w treści i w
formie, czyli przedstawia moje przemyślenia na stan wiedzy i poglądy z roku 2013. W chwili pisania tego wstępu
(luty 2025 r.) opisywane praktyki miały miejsce niespełna 12 lat wcześniej.

# Do dzieła!

Praktyki odbywamy w szkole średniej jako pomocnicy „adminów”. Przypada więc nam zarządzanie całym sprzętem i
siecią komputerową w placówce.

# Komputer do studia TV

Na miejscu zjawiłem się chwilę przed czasem. Zostaliśmy przywitani przez admina, który akurat w tym dniu ma
dyżur przed południem i od razu dostaliśmy zadanie do zrobienia – przygotować komputer do pracy w studiu
telewizyjnym. Czynność łatwa, gdyż polegała na zainstalowaniu Pinnacle Studio i sprawdzeniu, czy wszystko
działa oraz na przekopiowaniu danych ze starego peceta na nowy. Maszynka moim zdaniem na potrzeby amatorskiego
studia wystarczająca: Intel Core i5 3330, jakaś karta graficzna na chipie nVidia GeForce GT640 i 4 GB RAM‑u
DDR3.

# Przygotowania na maturę z j. polskiego

Ledwie zainstalowaliśmy tego Pinnacle’a, nagle dźwięk telefonu – dyrekcja prosi o przygotowanie czterech
laptopów lub komputerów oraz drukarkę na jutrzejszą maturę z j. polskiego (dla zdających, którzy tam jakiś
wniosek o to złożyli). No cóż, wybór padł na komputery – korporacyjne HP‑ki, nie pamiętam modelu. Szybciutko
udaliśmy się do magazynu, pobraliśmy cztery sztuki jednostek centralnych i monitorów. Wypakowaliśmy,
podłączyliśmy wszystko, uruchamiamy. Wita nas Windows 8 Pro. Wszystko fajnie, pojawił się jeden mały
problem. Wszystkie nasze myszy i klawiatury były na PS/2 i nie chciały współpracować, w ogóle zero
reakcji. Rozwiązanie okazało się ciekawe, trzeba było wejść do Setupu UEFI i przestawić tam opcję
odpowiedzialną za przydział IRQ – nie wiem dlaczego, ale zmiana przerwania na inne niż domyślne spowodowała
natychmiastową chęć współpracy peryferii z systemem.

Instalacja drukarki była raczej czynnością banalną, ot zwykłe Dalej, Dalej, Dalej, Zainstaluj, Zakończ, którą
trzeba było powtórzyć na wszystkich czterech komputerach – nie zdecydowaliśmy się na podpięcie tego
wszystkiego w sieć, gdyż to i tak na jutro tylko jest potrzebne, a 5‑ciometrowy kabel USB załatwił sprawę
dostępu do drukarki z każdego komputera.

# Podsumowanie

Wszystko to mimo, że raczej proste, idealnie zaangażowało nam 5 godzin pracy na praktykach. Później jeszcze
pozostaliśmy trochę poza programem spojrzeć trochę na serwery w dwóch pracowniach, ale to był tylko pierwszy
rzut oka, aby zaznajomić się z podstawowym zarządzeniem użytkownikami w domenie operowanej przez Windows
Server 2003.

To już wszystko na dziś. Ogólnie jesteśmy bardzo zadowoleni z pierwszego dnia na praktykach. Oboje zgodnie
twierdzimy, że dzień ten należy zaliczyć za udany.
