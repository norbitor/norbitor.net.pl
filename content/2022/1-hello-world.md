Title: Hello World
Date: 2022-10-10 23:28
Tags: luźne, offtopic, wstęp
Category: Pozostałe wpisy
Slug: hello-world
islug: hello-world

Kiedy zaczynam nowy program, projekt, czy nawet zwykły dokument, zazwyczaj wpisuję tam dwa proste słowa "hello world",
albo "witaj świecie". Nawet jeśli nie jest to szczególnie ambitne, wszak pierwszy znany program z taką treścią można
znaleźć w książce stanowiącej kanon współczesnej informatyki, czyli *The C Programming Language* Briana Kernighana i
Denisa Ritchiego. Pierwsze wydanie tej książki pochodzi z 1978 roku, zatem oficjalnie nasze programy oraz inne twory
informatyczne witają się ze światem już przynajmniej 44 lata! Myślę więc, że, pomimo braku oryginalności, rozpoczynanie
w ten sposób nowych przygód jest kultywowaniem pewniej tradycji.

Strona, na której właśnie jesteś, jest moim własnym, małym miejscem w Internecie. Domenę, jak można sprawdzić w
dowolnej usłudze typu *whois*, zarejestrowałem 12 października 2014 roku, czyli już 8 lat temu. Myślę, że to dobry
moment, aby w końcu zaczęło się tutaj coś dziać. Nawet jeśli będzie to niszowy blog, z moimi luźnymi przemyśleniami
oraz, co mnie mocno zmotywowało, materiałami dla studentów, z którymi mam przyjemność mieć zajęcia na Politechnice
Poznańskiej. Jak to się rozwinie dalej? Nie wiem, nie chcę też rzucać tutaj ogromnych planów, żeby nie spaliły one
na panewce. Szczególnie że zdarzało mi się w przeszłości spisane plany potraktować podświadomie jako zrealizowane.
Nie chcąc popełnić tego błędu, na ten moment zaproszę tylko do obserwowania od czasu do czasu tego miejsca. Wszak,
jedno mogę obiecać, będę sukcesywnie pracował nad tą stroną. Tak, aby służyła mi, ale też przede wszystkim wam.
