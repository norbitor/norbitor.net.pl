import platform

AUTHOR = 'Norbert Langner'
SITENAME = '/home/norbitor'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Warsaw'

DEFAULT_LANG = 'pl'
DEFAULT_CATEGORY = 'Pozostałe wpisy'
DEFAULT_METADATA = {
    'islug': '',
}
ARTICLE_TRANSLATION_ID = 'islug'

DATE_FORMATS = {
    'pl': ('pl_PL.utf8', '%d.%m.%Y'),
    'en': ('en_US.utf8', '%m/%d/%Y'),
}

ARTICLE_URL = '{lang}/{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_LANG_URL = ARTICLE_LANG_SAVE_AS = ARTICLE_SAVE_AS = ARTICLE_URL


MARKDOWN = {
    'output_format': 'html',
}

if platform.system() == 'Linux':
    STATIC_CREATE_LINKS = True

STATIC_PATHS = ['images', 'static']
EXTRA_PATH_METADATA = {'static/favicon.ico': {'path': 'favicon.ico'},}

OUTPUT_PATH = 'public/'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Wizytówka", "https://norbert.langner.net.pl"),
    ("Moja strona @ PUT", "https://www.cs.put.poznan.pl/nlangner"),
)

# Social widget
SOCIAL = (
    ("GitLab", "https://gitlab.com/norbitor/"),
    ("GitHub", "https://github.com/norbitor/"),
    ("LinkedIn", "https://www.linkedin.com/feed/"),
    ("Mastodon", "https://101010.pl/@norbitor"),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
